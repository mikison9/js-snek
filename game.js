var sides = {
	UP: 1,
	DOWN: 2,
	LEFT: 3,
	RIGHT: 4
};

var speed = 0;

var canMove = true;

var isRunning = false;

var isEnd = false;

var player = {
	direction: sides.RIGHT,
	body: [
		{
			x: 6,
			y: 5
		},
		{
			x: 5,
			y: 5
		}
	],
	jahudka: {
		x: 7,
		y: 7
	},
	score: 0
};

var scoreServer = "localhost:80";
var gameObj = document.getElementById("snek-game");
var gameStatus = document.getElementById("gamestatus");
var scores = document.getElementById("scores");
var playerNameInput = document.getElementById("player-name");
var ctx = gameObj.getContext("2d");

function draw() {
	clearCanvas();
	drawGrid();

	player.body.forEach(drawPlayerPiece);

	ctx.fillStyle = "red";
	ctx.fillRect(player.jahudka.x * 10, player.jahudka.y * 10, 10, 10);

	//ctx.fillRect(25, 25, 100, 100);
}

function setSpeed() {
	if (document.getElementById("radio-easy").checked) {
		speed = 500;
	} else if (document.getElementById("radio-medium").checked) {
		speed = 200;
	} else if (document.getElementById("radio-hard").checked) {
		speed = 100;
	}
}

function getScores() {
	var http = new XMLHttpRequest();
	var url = 'http://' + scoreServer + '/score/list';
	http.open('POST', url, true);

	http.onreadystatechange = function() {
    	if(http.readyState == 4 && http.status == 200) {
    		scores.innerHTML = http.responseText;
    	}
	}

	http.send();
}

function sendScore(name, score) {
	var http = new XMLHttpRequest();
	var url = 'http://' + scoreServer + '/score/add';
	http.open('POST', url, false);

	var formData = new FormData();

	formData.append("name", name);
	formData.append("score", score);

	http.send(formData);
}

function start() {
	if (!isEnd) {
		setSpeed();
		gameLoop();
	}
	
	isEnd = true;
}

function restart() {
	isEnd = false;
	setSpeed();
	player = {
		direction: sides.RIGHT,
		body: [
			{
				x: 6,
				y: 5
			},
			{
				x: 5,
				y: 5
			}
		],
		jahudka: {
			x: 7,
			y: 7
		},
		score: 0
	};
	
	gameStatus.innerHTML = "";
	
	gameLoop();
}

function clearCanvas() {
	ctx.fillStyle = "white"
	ctx.fillRect(0, 0, gameObj.width, gameObj.height);
}

function drawGrid() {
	for (x = 0; x < 151; x = x + 10) {
		drawLine(x, 0, x, 150);
	}

	for (y = 0; y < 151; y = y + 10) {
		drawLine(0, y, 150, y);
	}
}

function init() {
	getScores();
	draw();
}

function drawLine(originX, originY, newX, newY) {
	ctx.beginPath(); 
   	ctx.moveTo(originX,originY);
  	ctx.lineTo(newX,newY);
  	ctx.stroke();
}

function drawPlayerPiece(item, index) {
	ctx.fillStyle = "black";
	ctx.fillRect(item.x * 10, item.y * 10, 10, 10);
}

function getEmptyFields() {
	var fields = []

	for (intX = 0; intX < 15; intX++) {
		for (intY = 0; intY < 15; intY++) {
			if (!playerBodyContains({x: intX, y: intY}) && !isJahudka({x: intX, y: intY})) {
				fields.push({
					x: intX,
					y: intY
				});
			}
		}
	}

	return fields;
}

function isJahudka(position) {
	return player.jahudka.x == position.x && player.jahudka.y == position.y;
}

function playerBodyContains(position) {
	for(i = 0; i < player.body.length; i++) {
		if (player.body[i].x == position.x && player.body[i].y == position.y) {
			return true;
		}
	}
	return false;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function gameLoop() {
	if (!isRunning) {
		isRunning = true;
		while (true) {
			if (gameTick()) {
				sendScore(playerNameInput.value, player.score);
				getScores();
				break;
			}
			
			await sleep(speed);
		}
		isRunning = false;
	}
}

function gameTick() {
	canMove = true;
	if (checkPlayer()) {
		return true;
	}

	movePlayer();
	draw();
	return false;
}

function checkJahudka() {
	if (player.body[0].x == player.jahudka.x && player.body[0].y == player.jahudka.y) {
		player.score += 10;

		gameStatus.innerHTML = "score: " + player.score;

		genJahudka();

		return true;
	}

	return false;
}

function checkPlayer() {
	switch (player.direction) {
		case sides.UP:
			var nextPos = {
				x: player.body[0].x,
				y: player.body[0].y - 1
			}

			if (playerBodyContains(nextPos) || nextPos.y > 14 || nextPos.y < 0) {
				return true;
			}

			break;
		case sides.DOWN:
			var nextPos = {
				x: player.body[0].x,
				y: player.body[0].y + 1
			}

			if (playerBodyContains(nextPos) || nextPos.y > 14 || nextPos.y < 0) {
				return true;
			}

			break;
		case sides.LEFT:
			var nextPos = {
				x: player.body[0].x - 1,
				y: player.body[0].y
			}

			if (playerBodyContains(nextPos) || nextPos.x > 14 || nextPos.x < 0) {
				return true;
			}

			break;
		case sides.RIGHT:
			var nextPos = {
				x: player.body[0].x + 1,
				y: player.body[0].y
			}

			if (playerBodyContains(nextPos) || nextPos.x > 14 || nextPos.x < 0) {
				return true;
			}

			break;
	}

	return false;
}

function movePlayer() {
	/*for (i = player.body.length -1; i > 0; i--) {

		player.body[i].x = player.body[i - 1].x;
		player.body[i].y = player.body[i - 1].y;
	}*/

	var newHead = {
		x: 0,
		y: 0
	};

	switch (player.direction) {
		case sides.UP:
			newHead.x = player.body[0].x;
			newHead.y = player.body[0].y - 1;
			//player.body[0].y--;
			break;
		case sides.DOWN:
			newHead.x = player.body[0].x;
			newHead.y = player.body[0].y + 1;
			//player.body[0].y++;
			break;
		case sides.LEFT:
			newHead.x = player.body[0].x - 1;
			newHead.y = player.body[0].y;
			//player.body[0].x--;
			break;
		case sides.RIGHT:
			newHead.x = player.body[0].x + 1;
			newHead.y = player.body[0].y;
			//player.body[0].x++;
			break;
	}

	player.body.unshift(newHead)

	if (!checkJahudka()) {
		player.body.pop();
	}
}

function genJahudka() {
	var empty = getEmptyFields();

	var index = Math.floor(Math.random() * empty.length);

	player.jahudka.x = empty[index].x;
	player.jahudka.y = empty[index].y;
}

document.onkeydown = checkKey;

function checkKey(e) {

    e = e || window.event;

    if (e.keyCode == '38') {
    	// up arrow
        //console.log("up");
        goUp();
    }
    else if (e.keyCode == '40') {
        // down arrow
        //console.log("down");
        goDown();
    }
    else if (e.keyCode == '37') {
       	// left arrow
       	//console.log("left");
       	goLeft();
    }
    else if (e.keyCode == '39') {
       	// right arrow
       	//console.log("right");
       	goRight();
    }
    else if (e.keyCode == '13') {
    	restart();
    }

}

function goDown() {
	if (player.direction != sides.UP && canMove) {
        player.direction = sides.DOWN;
        canMove = false;
    }
}

function goUp() {
	if (player.direction != sides.DOWN && canMove) {
		player.direction = sides.UP;
		canMove = false;
	}
}

function goLeft() {
	if (player.direction != sides.RIGHT && canMove) {
		player.direction = sides.LEFT;
		canMove = false;
	}
}

function goRight() {
	if (player.direction != sides.LEFT && canMove) {
		player.direction = sides.RIGHT;
		canMove = false;
	}
}
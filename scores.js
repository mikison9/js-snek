var scoreServer = "34.250.15.33";

var scoresDiv = document.getElementById('scores');

var scrolldelay;

var scoredelay;

function refreshScores() {
	getScores();
	scoredelay = setTimeout(refreshScores, 1000);
}

function getScores() {
	var http = new XMLHttpRequest();
	var url = 'http://' + scoreServer + '/score/list';
	http.open('POST', url, true);

	http.onreadystatechange = function() {
    	if(http.readyState == 4 && http.status == 200) {
    		scoresDiv.innerHTML = http.responseText;
    	}
	}

	http.send();
}

function stopPageScroll() {
	clearTimeout(scrolldelay);
}

function pageScroll() {
    window.scrollBy(0,1);
    scrolldelay = setTimeout(pageScroll,10);

    var a = document.body.scrollTop;

	var b = document.body.scrollHeight - document.body.clientHeight;

    console.log(a / b);

    /*sleep(500);
    clearTimeout(scrolldelay);*/
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
